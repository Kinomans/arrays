﻿#include <iostream>
using namespace std;

int main()
{


    const int N = 2;
    int A[N][N] = { {0,1}, {2,3} };


    ///////////////
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            A[i][j] = 0;

    for (int i = 0; i < N / 2 + 1; i++)
        for (int j = 0; j <= i; j++)
            A[i][j] = 1;
    ///////////////////
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout << A[i][j] << " ";
        }
        cout << "\n";
    }
    return 0;


}

